const express= require('express');
var cors = require('cors')
const { dbConnection } = require ('../database/config')

class Server{

    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.usuariosPath = '/api/usuarios';

        this.conectarDB();

      
        //Middlewares, funciones que añaden funcionalidad
        this.middlewares();
        //Rutas de mi aplicacion
        this.routes();
    }
    

    async conectarDB(){
        console.log('etapa 1');
            await dbConnection();
    }


    middlewares(){
        this.app.use(cors())
        this.app.use(express.json());
        //directorio publico
        this.app.use(express.static('public'))
    }

    routes(){
        this.app.use(this.usuariosPath, require('../routes/usuarios'));
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto ', this.port)
        })
    }
}

module.exports = Server;