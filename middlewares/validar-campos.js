const { validationResult } = require('express-validator');

// next es lo que tenfgo que llamar si el middleware pasa
const validarCampos = ( req , res, next)  => {

    const errors = validationResult(req);
    if ( !errors.isEmpty()){
        return res.status(400).json(errors);
    }

    next();

}


module.exports = {
    validarCampos
}