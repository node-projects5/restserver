const mongoose = require('mongoose');

const dbConnection = async () => {

    try {

        console.log('conectando bd....');
        await mongoose.connect(process.env.MONGODB_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            // useCreateIndex: true,
            //  useFindAndModify: false
        });

        console.log('base de datos conectada');
        
    } catch (error) {
        console.log(error);
            throw new Error('Error en la conexion a bd');
        
    }

}


module.exports ={
    dbConnection
}