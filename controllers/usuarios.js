const { response, request } = require('express');
const Usuario = require('../models/usuario');
const bcryptjs = require('bcryptjs');


const usuariosGet  =  (req = request , res = response ) => {
    console.log('Llamada a GET');
    const query = req.query;
    //desestruturacion
    const {nombre, apellido = "Sin nombre"} = req.query;
    res.json({
        msg: 'get API - controlador ',
        nombre, apellido
    });
  }

 const usuariosPut =  async(req, res = response) => {

    const {id} = req.params;
    const {_id,password, google, correo, ...resto} = req.body;


    if (password){
          // encriptar la contraseña
          const salt = bcryptjs.genSaltSync();
          resto.password = bcryptjs.hashSync( password, salt)
    }

    const usuario = await Usuario.findByIdAndUpdate(id, resto)

    res.json({
        msg: 'put API - controlador', 
        usuario
    });
  }

 const usuariosPost = async (req, res = response ) => {

    const {nombre, correo, password, rol} = req.body;
    const usuario =  new Usuario({nombre,correo, password, rol});

    // verificar si el correo existe
    
    // encriptar la contraseña
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync( password, salt)

    // guardar la bd
    await usuario.save();

    // console.log(body)

    res.json({
        msg: 'post API - controlador!!!',
        usuario
    });
  }

 const usuariosDelete  = (req, res =  response) => {
    res.json({
        msg: 'delete API - controlador', body
    });
  }

  const usuariosPatch = (req, res = response) => {
    res.json({
        msg: 'patch API - controlador'
    });
  }

module.exports = {
    usuariosGet, usuariosPost, usuariosPut, usuariosPatch, usuariosDelete
}